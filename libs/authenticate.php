<?php
/**
  Copyright (C) 2023  cnngimenez

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
  PHP Version >7

  @category Main
  @package  Main
  @author   Christian Gimenez <christian.gimenez@fi.uncoma.edu.ar>
  @license  AGPLv3 https://gnu.org/licenses
  @link     https://gitlab.com/cnngimenez/que-dice

  @phpcs:disable PEAR.NamingConventions.ValidFunctionName.NotCamelCaps  
 */

require_once './config/config.php';    

/**
  Clase para acceder al authfile.

  El authfile contiene los nombres de usuarios y sus contraseñas
  hasheadas. Esta clase permite abrir este archivo y gestionarlo.

  @category Main
  @package  Main
  @author   Christian Gimenez <christian.gimenez@fi.uncoma.edu.ar>
  @license  AGPLv3 https://gnu.org/licenses
  @link     https://gitlab.com/cnngimenez/que-dice

  @SuppressWarnings(PHPMD.CamelCaseMethodName)
  @SuppressWarnings(PHPMD.CamelCasePropertyName)
  @SuppressWarnings(PHPMD.ElseExpressions)  
 */
class AuthFile
{

    /**
     El objeto File para leer y escribir.
     */
    private $_filename = null;

    /**
    Crear una instancia abriendo el archivo.
    
    @param $filename String Opcional. El path al archivo authfile.
                     Si no se provee utiliza el proveido por la
                     configuarción: AUTHFILE.

    @return AuthFile
     */
    function __construct($filename = AUTHFILE)        
    {
        $this->_filename = $filename;
    }
    
    /**
    Buscar el hash del usuario.
    
    @param $username String El nombre del usuario para buscar su hash.

    @return String el hash. Retornar false en caso de no encontrarse.
     */
    function find_hash($username)
    {
        $line = "";
        $lhash = "";
        $lusuario = "";

        $fauth = fopen($this->filename, 'r');
        
        while (!feof($fauth) && $username != $lusuario) {
            $line = fgets($fauth);
            
            $data = explode($line, "\",\"");
            $lusuario = substr($data[0], 1);
            $lhash = substr($data[1], strlen($data[1]) - 1);
        }
        
        fclose($fauth);

        if ($lusuario == $username) {
            return $lhash;
        } else {
            return false;
        }
        
    }

    function add_hash($username, $hash)
    {
        $fauth = fopen($this->_filename, 'a');
        fwrite($fauth, "\"$username\",\"$hash\"");
        fclose($fauth);
    }

    function add_login($username, $password)
    {
        $hash = password_hash($password, PASSWORD_BCRYPT);
        $this->add_hash($username, $hash);
    }
    
} // AuthFile

/**
  Authenticator class.

  @category Main
  @package  Main
  @author   Christian Gimenez <christian.gimenez@fi.uncoma.edu.ar>
  @license  AGPLv3 https://gnu.org/licenses
  @link     https://gitlab.com/cnngimenez/que-dice
  
  @SuppressWarnings(PHPMD.CamelCaseMethodName)
  @SuppressWarnings(PHPMD.CamelCasePropertyName)
  @SuppressWarnings(PHPMD.ElseExpressions)
 */
class Authenticator
{
    /**
    Instancia de AuthFile.
     */
    private $_authfile = null;

    const REGEXP_USERNAME = '/^[[:alnum:]_-\.]+$/';

    /**
      Crear una instancia de Authenticator.
      
      @param $authpath String El path del archivo de autenticación.
                       Opcional. Si no se brinda, utilizar AUTHFILE
                       de la configuración.
     */
    function __construct($authpath = AUTHFILE)
    {
        $this->_authfile = new AuthFile($authpath);
    }
    
    /**
    Chequear si el usuario y clave coincide con los registrados.

    @param $username String
    @param $password String
    
    @return Boolean Retorna true si se permite el login, false en caso
                    contrario.
     */
    function check_login($username, $password)
    {
        if (!$this->valid_user($username)) {
            return false;
        }
        
        $hash = $this->authfile->find_hash($username);

        if ($hash) {
            return password_verify($password, $hash);
        } else {
            return false;
        }
    }

    /**
    Agregar un login y guardarlo.

    @param $username String
    @param $password String

    @return Boolean Retorna true si el login fue guardado con éxito;
                    false en caso contrario.
     */
    function add_login($username, $password)
    {
        if (!$this->valid_user($username)) {
            return false;
        }

        $this->authfile->add_login($username, $password);
    }
    
    /**
    ¿El nombre de usuario es un string válido?

    @param $username String Nombre de usuario a validar.
    
    @return Boolean Retorna true si es válido; false si tiene un
                    caracter inválido.
     */
    function valid_user($username)
    {
        return preg_match($this->REGEXP_USERNAME, $username) != 0;
    }

} // Authenticator
