# Procesar un archivo de audio a texto

Esta página Web permite procesar un audio (WAV, OGG o MP3) a texto. Provee una interfaz Web sencilla para utilizar el software Speech-to-text de [Vosk](https://alphacephei.com/vosk/) y [Kaldi](https://github.com/alphacep/vosk-server/).

# Instalación

1. Instalar Vosk/Kaldi usando docker: `docker run -d -p 2700:2700 alphacep/kaldi-es:latest`
2. Instalar ffmpeg en el sistema
3. Clonar este repositorio en un directorio accesible por Apache/Nginx y PHP: `git clone https://gitlab.com/cnngimenez/que-dice.git`
4. `cd que-dice`
5. Instalar [composer](https://getcomposer.org/) y ejecutar `composer install`

Para el paso 3, si y solo si se está utilizando un entorno de desarrollo, se puede usar: `php -S localhost:8001`

# Licencia
Esta página se encuentra bajo la licencia GNU AFFERO GENERAL PUBLIC LICENSE version 3.
