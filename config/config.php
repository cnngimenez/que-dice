<?php
/**
  Copyright (C) 2023  cnngimenez

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.


  PHP version >7

  @category Main
  @package  Main
  @author   Christian Gimenez <christian.gimenez@fi.uncoma.edu.ar>
  @license  AGPLv3 https://gnu.org/licenses
  @link     https://localhost/
 */

/**
  Archivos soportados.

  Mime-types de los archivos que se permitirán procesar.
*/
const SOPORTADOS = [ "audio/x-wav", "audio/ogg", "audio/mpeg"];

/**
 Directorio donde se moverán los archivos de audio para su procesado.
 */
const DIR_AUDIOS = __DIR__ . '/uploads/';

/**
Token a utilizarse para la clave.

¡Este token debe ser único por cada instalación!
 */
const TOKEN = '';

/**
  ¿Dódne se encuentra el archivo de autenticación?
 */
const AUTHFILE = __DIR__ . '/config/authfile.txt';
