<?php
session_start();

if ($_SESSION['loggedin']) {
    $loggedin = true;
} else {
    $loggedin = false;
}
?>
    
<!DOCTYPE html>
<html lang="es">

  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Qué dice - Login</title>
    <meta content="Herramienta simple para procesar un audio a texto." name="description">
    <meta content="speech-to-text, audio, texto, asr" name="keywords">

    <link href="imgs/favico.png" rel="icon">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM"
          crossorigin="anonymous">
    <link href="css/index.css" rel="stylesheet">
  </head>
  <body>
    <main>
      <div class="container">
        <h1>Iniciar sesión</h1>
        <p>
          Al iniciar sesión, no se solicitará un CAPTCHA por cada audio a procesar.
        </p>
        <?php if (!$loggedin) { ?>
        <form>
            <div class="mb-3">
              <label for="usuario" class="form-label">Usuario:</label>
              <input type="text" class="form-control" id="usuario" name="usuario"
                     placeholder="Usuario">
            </div>
            <div class="mb-3">
              <label for="clave" class="form-label">Contraseña:</label>
              <input type="password" class="form-control" id="clave" name="clave"
                     placeholder="Contraseña">
            </div>

            <button class="btn btn-primary" id="btn-login" disabled>
              Iniciar Sesión
            </button>
        </form>

        <div class="alert alert-success" style="display:none" id="login-feedback">
          <p>
            Usted ha iniciado sesión con éxito.
          </p>
          <p>
            No se utilizará el CAPTCHA en la página principal.
          </p>
        </div>
        
        <?php } else { ?>
          <div class="mb-3 alert alert-warning">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                 fill="currentColor" class="bi bi-exclamation-triangle-fill" viewBox="0 0 16 16"
                 aria-hidden="true">
              <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
            </svg>
            Usted ya ha iniciado sesión.
          </div>
        <?php } ?>

        <div class="my-3">
          <a href="index.php" class="btn btn-secondary">Volver a la página principal</a>
        </div>
      </div>
    </main>

    <?php if (!$loggedin) { ?>
      <script src="js/login.js" type="module"></script>
    <?php } ?>
  </body>
</html>
