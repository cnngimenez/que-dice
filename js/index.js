/*
  Copyright (C) 2023  cnngimenez

  index.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/**
 Mime-type aceptados para los archivos a subir.
 
 Los archivos con estos Mime-types serán permitidos subir.
 */
const accepted_filetypes = [
    'video/ogg', // JS confunde audio por video...
    'audio/ogg',
    'audio/x-wav',
    'audio/mpeg'
];

/**
 Tamaño máximo permitido del archivo.
 */
const max_filesize = 10000000; //10485760; // 10485760B = 10 MB

function show_error(visible, message) {
    let div = document.querySelector('div.error');
    let msg = document.querySelector('#error');

    if (visible) {
        div.style.display = 'block';
    } else {
        div.style.display = 'none';
    }
    
    if (message != undefined && message != null) {
        msg.innerText = message;
    }
}

function show_loading(visible) {
    let div = document.querySelector('div.waiting');
    if (visible) {
        div.style.display = 'block';
    } else {
        div.style.display = 'none';
    }
}

function show_responses(visible, json) {
    let elt = document.querySelector('pre#texto-resultado');
    let div = document.querySelector('div.resultado');

    if (visible) {
        div.style.display = 'block';
    } else {
        div.style.display = 'none';
    }

    if (json != undefined && json != null) {
        json.forEach( (text) => {
            elt.innerText += text + "\n";
        });
    }
}

/**
 ¿El archivo file es aceptado para ser enviado al servidor?

 @param file File El archivo a cargar.

 @return Boolean true si es aceptado.
 */
function filetype_aceptado(file) {
    
    return file != undefined &&
        file != null && 
        accepted_filetypes.find( (mime) => {
            return mime == file.type;
        }) != undefined;
}

function filesize_aceptado(file) {
    return file != undefined &&
        file != null &&
        file.size < max_filesize;
}

async function enviar_archivo(file, captcha) {
    const formdata = new FormData();
    formdata.append('archivo', file);
    formdata.append('captcha', captcha);

    try {
        const response = await fetch('stt.php', {
            method: 'POST',
            body: formdata
        });

        if (response.ok) {
            return response.json();
        } else {
            // Error 404 o similar.
            console.error("El servidor respondió el código de error: " + response.status);
            throw new Error("El servidor respondió el código de error: " + response.status);
        }
    } catch (error) {
        // CORS o problema en la conexión.
        console.error("Error al enviar el archivo: ", error);        
        throw new Error("Error al enviar el archivo: " + error.message);
    }
}

/**
 Mostrar la validación del archivo.

 Si es válido, mostrar el input en verde. Si no lo es, mostrar mensaje
 de error.

 @param valid Boolean true si es válido. 
 */
function update_file_validation(valid_type, valid_size) {
    let input = document.querySelector('input#archivo');
    let feedback = document.querySelector('div#archivo-invalido');
   
    if (valid_type && valid_size) {
        feedback.style.display = 'none';
        input.classList.remove('is-invalid');
        input.classList.add('is-valid');
    } else {
        feedback.style.display = 'block';
        input.classList.add('is-invalid');
        input.classList.remove('is-valid');
    }

    var errormsg = "";
    if (!valid_type) {
        errormsg += "¡No se acepta este formato de archivo!\n";
    }
    if (!valid_size) {
        errormsg += "¡El tamaño del archivo supera el límite máximo!\n";
    }
    feedback.innerText = errormsg;
}

function update_btn_enviar(valid) {
    let button = document.querySelector('button#btn-enviar');
    
    button.disabled = valid;
}

function update_form(first_run) {
    let input = document.querySelector('input#archivo'); 
    let tipo_aceptado = filetype_aceptado(input.files[0]);
    let size_aceptado = filesize_aceptado(input.files[0]);
    
    update_btn_enviar(input.files.length == 0 ||
                      !tipo_aceptado || !size_aceptado);

    if (first_run == undefined || first_run == null || !first_run) {
        update_file_validation(tipo_aceptado, size_aceptado);
    }
}

function on_btn_enviar_clicked(){
    let input = document.querySelector('input#archivo');
    let captcha_input = document.querySelector('input#captcha');
    
    input.innerText = "";
    
    show_loading(true);
    show_error(false);
    enviar_archivo(input.files[0], captcha_input.value)
        .then( (json) => {
            console.log(json);
            show_loading(false);
            show_responses(true, json);
        })
        .catch( (error) => {
            show_loading(false);
            show_error(true, error);
        });
}

function on_input_archivo_changed(){
    update_form(false);
}

function assign_handlers(){
    var elt = document.querySelector('button#btn-enviar');
    elt.addEventListener('click', on_btn_enviar_clicked);

    elt = document.querySelector('input#archivo');
    elt.addEventListener('change', on_input_archivo_changed);
}


function startup() {
    assign_handlers();

    // A veces el formulario se recarga con datos.
    update_form(true);
    
    show_loading(false);
}

if (document.readyState !== 'loading') {
    startup();
} else {
    document.addEventListener('DOMContentLoaded', startup);
}

export {enviar_archivo};
