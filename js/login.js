/*
  Copyright (C) 2023  cnngimenez

  login.js

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

function show_login_feedback(visible) {
    let div = document.querySelector('div#login-feedback');
    if (visible) {
        div.style.display = 'block';
    } else {
        div.style.display = 'none';
    }
}

function on_btn_login_clicked() {
    fetch('./do_login.php', {
        method: 'POST',
        data: {
            usuario: usuario,
            clave: clave
        }} )
        .then( () => {
            show_login_feedback(true);
        });
}

function update_btn_login() {
    let input_usuario = document.querySelector('input#usuario');    
    let input_clave = document.querySelector('input#clave');
    let btn_login = document.querySelector('button#btn-login');

    btn_login.disabled = input_usuario.value == "" ||
        input_clave.value == "";
}

function assign_handlers(){
    var elt = document.querySelector('input#usuario');
    elt.addEventListener('input', update_btn_login);
    
    elt = document.querySelector('input#clave');
    elt.addEventListener('input', update_btn_login);

    elt = document.querySelector('button#btn-login');
    elt.addEventListener('click', on_btn_login_clicked);
}

function startup() {
    assign_handlers();
    show_login_feedback(false);
}

if (document.readyState !== 'loading') {
    startup();
} else {
    document.addEventListener('DOMContentLoaded', startup);
}

// export {};
