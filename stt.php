<?php
/**
  Copyright (C) 2023  cnngimenez

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.


  PHP version >7

  @category Main
  @package  Main
  @author   Christian Gimenez <christian.gimenez@fi.uncoma.edu.ar>
  @license  AGPLv3 https://gnu.org/licenses
  @link     https://localhost/
 */

require_once  "./vendor/autoload.php";
require_once  "./config/config.php";

use WebSocket\Client;
use Gregwar\Captcha\CaptchaBuilder;

session_start();

// --------------------------------------------------
// Precondiciones

if (!isset($_SESSION['captcha'])) {    
    die('["Error: No se ha utilizado ningún CAPTCHA"]');
}
if (empty($_POST) || !isset($_POST['captcha']) || !isset($_FILES['archivo'])) {
    die('["Error: No se han brindado suficientes parámetros"]');
}

/**
 Resultados del procesado.
 */
$results = [];

/**
  Builder para generar y testear el CAPTCHA.
 */
$captchab = new CaptchaBuilder($_SESSION['captcha']);
$captchab->build();

/**
Almacenar el resultado de $output si no es un parcial.
  
Un resultado es un JSON que tiene un atributo "result". Estos son
almacenados para trataros luego.

Función con side-effect: Modifica $results.

@param $output String La salida de Kaldi.

@return No tiene salida.
 */
function store_results($output)
{
    global $results;
    
    $json = json_decode($output, true);
    if (isset($json['result'])) {
        $results[] = $json['text'];
    }
}

/**
Convertir los archivos de cualquier formato a WAV.

Se genera un nuevo archivo WAV con un nombre único para evitar
colisiones.

@param $file String el nombre del archivo a convertir.

@return El nombre del archivo WAV generado.
 */
function convert_wav($file)
{
    $filestr = escapeshellcmd($file);
    $wavfile = uniqid() . ".wav";
    $strcommand = "ffmpeg  -i $filestr  -ac 1  -acodec pcm_s16le -ar 44100 $wavfile";

    shell_exec($strcommand);

    return $wavfile;
}

/**
¿Es el nombre del archivo (extensión) soportado?

@param $filename El nombre del archivo con extensión.

@return true si se acepta el formato del archivo.
 */
function aceptar_filetype($filename)
{
    return in_array(mime_content_type($filename), SOPORTADOS);
}

/**
Chequear si el CATPCHA coincide con el esperado.

@param $input String Los caracteres ingresadas por el usuario.

@return Boolean. True si es el captcha esperado.
 */
function captcha_esperado($input)
{
    global $captchab;
    
    return $captchab->testPhrase($input);
}

// --------------------------------------------------
// Main procedure

// Chequear captcha
if (!captcha_esperado($_POST['captcha'])) {
    die('["Error: ¡Captcha no coincide con el esperado!"]');
}

/**
Path y nombre del archivo de audio cargado por el usiario a procesar.
*/
$fichero_subido = DIR_AUDIOS . uniqid() . basename($_FILES['archivo']['name']);

if (!move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido)) {
    die('["Error: ¡No se pudo porcesar el archivo!"]');
}

// Chequear el tipo del archivo.
if (!aceptar_filetype($fichero_subido)) {
    die('["Error: ¡El tipo de archivo no es aceptado!"]');
}

/*
 echo 'Más información de depuración:';
 print_r($_FILES);
*/

// Convertir a WAV
$wavfile = convert_wav($fichero_subido);

// Enviar al Speech-to-text
$client = new Client("ws://localhost:2700/", array('timeout' => 2000));
$myfile = fopen($wavfile, "rb");

$s = '{"config" : { "sample_rate": 44100 } }';
$client->send($s);

while (!feof($myfile)) {
    $data = fread($myfile, 8000);
    $client->send($data, 'binary');
    store_results($client->receive() . "\n");
}

$s = "{\"eof\" : 1}";
$client->send($s);

store_results($client->receive() . "\n");

fclose($myfile);

// Limpiar archivos.
unlink($archivo_subido);
unlink($wavfile);

// Enviar resultados.
header('Content-Type: application/json', true);
echo json_encode($results);
