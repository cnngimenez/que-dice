<?php
require_once './vendor/autoload.php';
use Gregwar\Captcha\CaptchaBuilder;

session_start();

$builder = new CaptchaBuilder;
$builder->build();

$_SESSION['captcha'] = $builder->getPhrase();
?>

<!DOCTYPE html>
<html lang="es">

  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Qué dice</title>
    <meta content="Herramienta simple para procesar un audio a texto." name="description">
    <meta content="speech-to-text, audio, texto, asr" name="keywords">

    <link href="imgs/favico.png" rel="icon">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM"
          crossorigin="anonymous">
    <link href="css/index.css" rel="stylesheet">
  </head>
  <body>
    <main>
      <div class="container">
        <h1>Procesar audio a texto</h1>

        <p>
          Seleccione un audio a procesar. Solo se permiten WAV, OGG y MP3.
          El archivo no debe superar los 10MB.
        </p>

        <form>
          <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />

          <div class="mb-3">
            <label for="archivo" class="form-label">Seleccionar audio a procesar:</label>
            <input class="form-control" name="archivo" type="file" id="archivo">
          </div>
          <div class="invalid-feedback" id="archivo-invalido">
          </div>

          <div class="mb-3">
            <label for="captcha" class="form-label">Indique las letras que vé en la imágen:</label>
            <img src="<?php echo $builder->inline(); ?>" />          
            <input type="text" class="form-control" id="captcha" name="captcha" placeholder="CAPTCHA">
          </div>
          
        </form>
        <div class="my-3">       
          <button class="btn btn-primary" id="btn-enviar" disabled>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                 fill="currentColor" class="bi bi-send-fill" viewBox="0 0 16 16"
                 aria-hidden="true">
              <path d="M15.964.686a.5.5 0 0 0-.65-.65L.767 5.855H.766l-.452.18a.5.5 0 0 0-.082.887l.41.26.001.002 4.995 3.178 3.178 4.995.002.002.26.41a.5.5 0 0 0 .886-.083l6-15Zm-1.833 1.89L6.637 10.07l-.215-.338a.5.5 0 0 0-.154-.154l-.338-.215 7.494-7.494 1.178-.471-.47 1.178Z"/>
            </svg>
            Enviar audio
          </button>
        </div>
        <div class="mb-3 waiting">
          <p>
            Procesando audio...
          </p>
          <img src="imgs/Loading_icon.webp" />
        </div>

        <div class="alert alert-danger error">
          <b>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                 fill="currentColor" class="bi bi-exclamation-triangle-fill" viewBox="0 0 16 16"
                 aria-hidden="true">
              <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
            </svg>
            Ha surgido un inconveniente:
          </b>
          <p id="error"></p>
          <p>
            Por favor, comuníquese con el administrador del sitio al correo electrónico:
            cnngimenez arroba fi.uncoma.edu.ar
          </p>
          <p>
            También puede reportar el error en la plataforma GitLab:
            <a href="https://gitlab.com/cnngimenez/que-dice/-/issues">
              Reportar error en las "Issues" de GitLab
            </a>
          </p>
          <p>
            <a href="/" class="btn btn-primary">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                   fill="currentColor" class="bi bi-arrow-repeat" viewBox="0 0 16 16"
                   aria-hidden="true">
                <path d="M11.534 7h3.932a.25.25 0 0 1 .192.41l-1.966 2.36a.25.25 0 0 1-.384 0l-1.966-2.36a.25.25 0 0 1 .192-.41zm-11 2h3.932a.25.25 0 0 0 .192-.41L2.692 6.23a.25.25 0 0 0-.384 0L.342 8.59A.25.25 0 0 0 .534 9z"/>
                <path fill-rule="evenodd" d="M8 3c-1.552 0-2.94.707-3.857 1.818a.5.5 0 1 1-.771-.636A6.002 6.002 0 0 1 13.917 7H12.9A5.002 5.002 0 0 0 8 3zM3.1 9a5.002 5.002 0 0 0 8.757 2.182.5.5 0 1 1 .771.636A6.002 6.002 0 0 1 2.083 9H3.1z"/>
              </svg>
              Recargar página
            </a>
          </p>
        </div>

        <div class="resultado">
          <h2>Resultado</h2>
          <pre id="texto-resultado">
          </pre>
        </div>           

      </div>
    </main>

    <footer>
      <div class="container my-5">
        <p>
          Este software se encuentra bajo la licencia GNU Affero General Public License version 3.
        </p>
        <p>
          <a href="https://gitlab.com/cnngimenez/que-dice">Ver código fuente</a>.
        </p>
        <p>
          <a href="https://gitlab.com/cnngimenez/que-dice/-/issues">Reportar sugerencias o errores</a>.
        </p>
      </div>
    </footer>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
            crossorigin="anonymous"></script>
    <script src="js/index.js" type="module">
    </script>
  </body>
</html>
